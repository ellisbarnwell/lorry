#!/bin/sh
#
# Tests that all refs are pushed if no push-refspecs are given
#
# Copyright (C) 2012  Codethink Limited
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

set -e

git_upstream="$DATADIR"/git-upstream
logfile="$DATADIR"/no-pushspec-pushall.log
workdir="$DATADIR"/work-dir

lorryfile="$DATADIR"/no-pushspec.lorry
cat >"$lorryfile" <<EOF
{
    "no-pushspec": {
        "type": "git",
        "url": "file://$git_upstream"
    }
}
EOF

mirror_path="$DATADIR"/git-mirror
mkdir -p "$mirror_path"
git init --quiet --bare "$mirror_path"/no-pushspec.git

"${SRCDIR}/test-lorry" --log="$logfile" --working-area="$workdir" \
        --mirror-base-url-push=file://"$mirror_path" \
        --mirror-base-url-fetch=file://"$mirror_path" \
        "$lorryfile"

# verify that the git repository has all the refs that upstream does
test "$(cd "$mirror_path"/no-pushspec.git && git for-each-ref | sort)" = \
     "$(cd "$git_upstream" && git for-each-ref | sort)"
