#!/bin/sh
#
# Creates a bzr repository with a single file and a single commit.
#
# Copyright (C) 2011-2012, 2020  Codethink Limited
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


set -e

# If bzr is not available and brz is, use brz instead
if ! command -v bzr >/dev/null && command -v brz >/dev/null; then
    bzr() { brz "$@"; }
fi

# create the repository
repo="$DATADIR/bzr-test-repo"
mkdir "$repo"
cd "$repo"
bzr init --quiet trunk

# add the test file
cd "$repo/trunk"
echo "first line" > test.txt
bzr add --quiet test.txt

# make a commit
bzr commit --quiet -m "first commit"

# create the .lorry file for the repository
cat <<EOF > $DATADIR/bzr-test-repo.lorry
{
  "bzr-test-repo": {
    "type": "bzr",
    "url": "file://$repo/trunk"
  }
}
EOF


# create the working directory
test -d "$DATADIR/work-dir" || mkdir "$DATADIR/work-dir"
