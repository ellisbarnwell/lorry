#!/bin/sh
#
# Tests converting a simple CVS repository to git.
#
# Copyright (C) 2012, 2020  Codethink Limited
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


set -e

logfile="$DATADIR/cvs-test-repo.log"
workdir="$DATADIR/work-dir"

normalize() {
    sed -r -e '/hooks\/.*\.sample/d' \
           -e "s/pack-[0-9a-z]+\.(idx|pack)$/pack-file/" \
           -e "/\/objects\/info\/commit-graph$/d" \
           -e "/\/objects\/pack\/pack-[0-9a-z]+\.bitmap$/d" \
           -e "/\/\.cvsps\//d" \
           -e "s|$DATADIR|DATADIR|g" "$@"
}

# CVS wants $USER, $LOGNAME, and $LOGNAME set to a real username.
export USER=root
export LOGNAME=$USER
export USERNAME=$USER

"${SRCDIR}/test-lorry" --pull-only --log="$logfile" --working-area="$workdir" \
  "$DATADIR/cvs-test-repo.lorry" > /dev/null

# verify that the git repository was created successfully
(
  cd "$workdir/cvs-test-repo/git-a/"

  echo "branches:"
  git show-ref | cut -d' ' -f2 | LC_ALL=C sort

  echo "test file:"
  git cat-file blob master:test.txt

  echo "commit messages:"
  git log --pretty='%s' master
)

find "$workdir/cvs-test-repo" | LC_ALL=C sort | normalize
