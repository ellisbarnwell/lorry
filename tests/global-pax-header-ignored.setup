#!/bin/sh
#
# Creates tarballs constructed with a global PAX header.
#
# Copyright (C) 2021  Ben Brown
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


set -e

dir="${DATADIR}/tar-global-pax-header"
mkdir -p "${dir}"
touch "${dir}"/test

tar_path="${DATADIR}/global-pax-header.tar"
tar --pax-option="globexthdr.name=pax_global_header" \
    --pax-option="foo=bar" \
    -C "${DATADIR}" -cf "${tar_path}" "$(basename "${dir}")"

# create the .lorry file for the repository
cat <<EOF > "${DATADIR}/global-pax-header.lorry"
global-pax-header:
  type: tarball
  url: file://${tar_path}
EOF

# create the working directory
mkdir -p "${DATADIR}/work-dir"
